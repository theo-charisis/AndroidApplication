package com.example.healthyapplication;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {
    private Menu menu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first);
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_main, menu);
//        this.menu = menu;
//        return super.onCreateOptionsMenu(menu);
//    }


// double tap back button to exit application
boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }


public void startquiz(View o) {
    Intent intent = new Intent(this, QuestionActivity.class);
    startActivity(intent);
}
    public void facts(View o) {
        Intent intent = new Intent(this, facts.class);
        startActivity(intent);
    }
    public void memo(View o) {
        Intent intent = new Intent(this, MemoActivity.class);
        startActivity(intent);
    }

}
