package com.example.healthyapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.TextView;


public class NextStage extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nextstage);
        TextView textResult = (TextView) findViewById(R.id.textComplete);
        Bundle b = getIntent().getExtras();
        int score = b.getInt("score");
        textResult.setText("Final score: " + " " + score + ". First level completed!.");
    }

    public void nextlevel(View o) {
        Intent intent = new Intent(this, QuestionActivity2.class);
        startActivity(intent);
    }

    public void mainmenu(View o) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}