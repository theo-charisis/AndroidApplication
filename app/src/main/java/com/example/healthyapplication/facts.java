package com.example.healthyapplication;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * Created by ΤΕΟ on 19/5/2017.
 */

public class facts  extends Activity {



    ListView list;
    String[] name = {
            "Tomato",
            "Cherry",
            "Coconuts",
            "Banana",
            "Apples",
            "Dragon Fruit",
            "Orange",
            "Blackberries",
            "Avocado",
            "Kiwi",
            "Pineapple",
            "Peach",
            "Pomegranate"
    } ;

    String[] details = {
            "Tomatoes were erroneously thought to be poisonous (although the leaves are) by Europeans who were suspicious of their bright, shiny fruit. ",
            "Cherry contains the highest concentration of sugars.Farmers use the help of helicopters to air-dry their trees, this prevents the cherries from splitting.",
            "In Thailand, farmers train monkeys to climb up the Coconut trees, pick the fruit and drop it down to them.\"Coco,\" meaning \"monkey face,\" was the name given to this tropical food by Spanish explorers because of the three indentations on the bottom. But these indentations are the key for opening the coconut successfully. Insert a screwdriver into the softest hole to drain the liquid into a bowl, and then use a sturdy knife to circumvent the coconut's seam a few times. Give it a sharp \"thwack\" on a hard surface to break it into two pieces. Voilà!",
            "Bananas probably originated in Malaysia; transported by early explorers to India, where they were first referenced in sixth century BCE Buddhist writings. Alexander the Great tried his first banana while on campaign in India and is said to have brought the fruit to the Western world",
            "In Greek mythology the apple was the symbol for love; in the Bible in the Garden of Eden it was the symbol for knowledge. According to archaeologists and historians, apples originated in the Middle East more than 5,000 years ago and have been grown in the UK for at least 1,000 years.More than 80% of an apple is water. Carbohydrates make up about 10% while minerals and vitamins take up 4%. There are about 40 calories in an average apple but no fat. While most of an apple’s fibre content is in the peel there’s also some in the core. However, if you eat the core, remember that apple pips contain traces of cyanide – but not enough to poison you! But that explains the bitter taste. Another reason to eat the peel of an apple – after washing it – is that most of the apple’s vitamin C is just under the skin.",
            "The main reason for dragon fruit's preciousness is that it lives only one night! First, a climbing cactus produces a beautiful pink or yellow flower. Sometimes called \"moonflower\" or \"Queen of the night,\" the plant blooms from evening to midnight, only to wither in strong sunlight. During the night, the pungent flowers are pollinated by moths and bats. Although the flower dies, the cactus bears pitya fruit about six times every year.",
            "Texts referencing sweet oranges date to 1472, when a trader from Savona, Italy, sent an invoice to a client for the sale of 1,000 sweet oranges. Genoa and Venice (both independent in earlier centuries) sailed trading routes from Italy through Palestine, Arabia, and India at great cost, opening trade and new Far Eastern discoveries at the end of the 15th century, including oranges.",
            "There is an old Irish proverb saying: “On Michaelmas Day, the devil puts his foot on blackberries.” According to British and Irish superstition, Old Michaelmas Day (the Feast of St. Michael, one of the principal angelic warriors), which is on October 10th, is the last day when blackberries should be harvested. Legend has it that this was the day Lucifer was banished from heaven and, upon falling from the skies, landed on a thorny blackberry bush. He cursed and spat on the fruits, then scorched them with his fiery breath, making them unfit for human consumption.",
            "Because ancient Aztecs considered avocados a fertility fruit, and the Mayans used them as an aphrodisiac, a stigma against the fruit carried clear through the 19th century. Growers finally launched a campaign to convince consumers they could eat avocados without compromising themselves.",
            "Placing kiwifruits in a brown paper bag for four to six days will help them ripen. Keeping them in a paper bag with an apple, banana, or pear will speed up the ripening process even more.This fuzzy brown powerhouse with the bright green flesh also is rich in vitamin A, K, E and B, potassium, copper, folate, and fiber. The health benefits kiwifruits provide translate into protection against several cancers, coronary heart disease and the risk of stroke, potential relief from diverticulitis and irritable bowel syndrome, and support for pregnant mothers.",
            "Grow your own pineapple plant at home by twisting the crown off a store-bought pineapple, drying it for two to three days, and planting it. However, while pineapple plants can produce fruit for as long as 50 years in the wild, it takes two years to yield one fruit. Pineapples are good for you, no matter your age. Adults gain bone support, and children who eat it get what they need for bone development, just one of the great benefits of this tropical fruit.",
            "Peaches have been on a long journey through the ages.Through the ancient Persian silk route, trees were carried for cultivation in Europe. The Romans called them “Persian apples” after the country that introduced them.\n" + "\n" + "Spaniards introduced peaches to South America, and the French brought them to Louisiana. Columbus brought peach trees to America on his second and third voyages, followed by colonists who took them from England to grow in their new American home.",
            "During traditional Iranian weddings, a basket of pomegranates is placed on the ceremonial cloth to symbolize wishes for a joyous future. Following Turkish marriage ceremonies, the bride casts a pomegranate on the ground, and the number of arils that spill out are said to indicate the number of children the couple will have. Symbolizing prosperity and abundance in virtually every civilization.\n" + "\n" + "Not only delicious and versatile, pomegranates are one of the healthiest foods you can eat."
} ;

    Integer[] imageId = {
            R.drawable.image2,
            R.drawable.image3,
            R.drawable.image4,
            R.drawable.image5,
            R.drawable.image6,
            R.drawable.image8,
            R.drawable.image1,
            R.drawable.image9,
            R.drawable.image10,
            R.drawable.image11,
            R.drawable.image12,
            R.drawable.image13,
            R.drawable.image14
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facts);

        CustomList adapter = new
                CustomList(facts.this, name,details, imageId);
        list=(ListView)findViewById(R.id.list);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //Toast.makeText(facts.this, "You Clicked at " +name[+ position], Toast.LENGTH_SHORT).show();

            }
        });

    }

}
